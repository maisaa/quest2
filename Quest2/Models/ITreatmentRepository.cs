﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest2.Models
{
   public interface ITreatmentRepository
    {
        IEnumerable<LU_Treatment> Treatment { get; }

        

        void AddTreatments(LU_Treatment treatments);

        IEnumerable<LU_Treatment> GetAllTreatments();

        LU_Treatment GetTreatmentById(int treatmentId);

    }
}
