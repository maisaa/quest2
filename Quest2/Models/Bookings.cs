﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quest2.Models
{
    public class Bookings
    {
        public int BookingId { get; set; }

        public string Name { get; set; }
        
        //[Required, Phone, Display(Name = "Phone number")]
        [Required(ErrorMessage = "You must provide a Mobile number")]
        [Display(Name = "Mobile")]
        [DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }

        public int TreatmentId { get; set; }

        public int TimeSlotId { get; set; }

        public string Note { get; set; }

        [Required]
        [RegularExpression("^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9]+)*\\.([a-z]{2,4})$", ErrorMessage = "Invalid email format.")]
        //[EmailAddress(ErrorMessage ="Invalid Email Format")]
        public string Email { get; set; }

        [Required, DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = @"{0:dd\/MM\/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BookedDate { get; set; }

        public bool IsDeleted { get; set; }

        public  LU_TimeSlot TimeSlots { get; set; }

        public  LU_Treatment Treatments { get; set; }
        
    }
}
