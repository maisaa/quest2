﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest2.Models
{
   public interface ITimeSlotRepository
    {

        IEnumerable<LU_TimeSlot> TimeSlots { get; }

        void AddTimeSlots(LU_TimeSlot timeSlots);

        IEnumerable<LU_TimeSlot> GetAllTimeSlots();

        LU_TimeSlot GetTimeSlotById(int timeSlotId);


    }
}
