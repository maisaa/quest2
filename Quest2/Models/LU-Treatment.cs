﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest2.Models
{
    public class LU_Treatment
    {
       public  int    TreatmentId { get; set; }
       public  string TreatmentTitle { get; set; }
       public  string TreatmentDescription { get; set; }


        public virtual ICollection<Bookings> Bookingss { get; set; }
    }
}
