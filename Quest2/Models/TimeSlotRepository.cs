﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest2.Models
{
    public class TimeSlotRepository : ITimeSlotRepository
    {
        private readonly AppDbContext _appDbContext;


        public TimeSlotRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<LU_TimeSlot> TimeSlots { get; set; }

        public void AddTimeSlots(LU_TimeSlot timeSlots)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LU_TimeSlot> GetAllTimeSlots()
        {
            return _appDbContext.TimeSlots;
        }

        public LU_TimeSlot GetTimeSlotById(int timeSlotId)
        {
            return _appDbContext.TimeSlots.FirstOrDefault(p => p.TimeSlotId == timeSlotId);
        }

       
    }
}
