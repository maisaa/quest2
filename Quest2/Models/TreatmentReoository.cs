﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest2.Models
{
    public class TreatmentReoository : ITreatmentRepository
    {
        public IEnumerable<LU_Treatment> Treatment { get; set; }

        private readonly AppDbContext _appDbContext;


        public TreatmentReoository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }


        public void AddTreatments(LU_Treatment treatments)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LU_Treatment> GetAllTreatments()
        {
            return _appDbContext.Treatments;
        }

        public LU_Treatment GetTreatmentById(int treatmentId)
        {
            return _appDbContext.Treatments.FirstOrDefault(p => p.TreatmentId == treatmentId);
        }
    }
}