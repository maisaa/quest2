﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest2.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<Bookings> Bookingss { get; set; }
        public DbSet<LU_TimeSlot> TimeSlots { get; set; }
        public DbSet<LU_Treatment> Treatments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bookings>(entity =>
            {
                entity.HasKey(e => e.BookingId);

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Mobile)
                    .HasMaxLength(14)
                    .IsUnicode(false);


                entity.HasOne(e => e.TimeSlots)
               .WithMany(e => e.Bookingss)
               .HasForeignKey(o => o.TimeSlotId)
               .IsRequired();

                entity.HasOne(e => e.Treatments)
                .WithMany(e => e.Bookingss)
                .HasForeignKey(o => o.TreatmentId)
                .IsRequired();
            });

            modelBuilder.Entity<LU_TimeSlot>(entity =>
            {
                entity.HasKey(e => e.TimeSlotId);
            });

            modelBuilder.Entity<LU_Treatment>(entity =>
            {
                entity.HasKey(e => e.TreatmentId);
            });
        }
    }
}
