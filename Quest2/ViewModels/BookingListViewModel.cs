﻿using Quest2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest2.ViewModels
{
    public class BookingListViewModel
    {
        public IEnumerable<Bookings> Bookingss { get; set; }

        public IEnumerable<LU_TimeSlot> TimeSlots { get; set; }

        public IEnumerable<LU_Treatment> Treatments { get; set; }

        public string TreatmentTitle { get; set; }

        public int TimeSlotTitle { get; set; }
    }
}
